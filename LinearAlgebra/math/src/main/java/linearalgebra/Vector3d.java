// YOU RAN WANG 2233844
package linearalgebra;
import java.lang.Math;

public class Vector3d 
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x,double y,double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public double dotProduct(Vector3d inputVector){
        return this.x * inputVector.getX() + this.y * inputVector.getY() + this.z * inputVector.getZ();
    }

    public Vector3d add(Vector3d inputVector){
        Vector3d newVector = new Vector3d((this.x + inputVector.getX()), (this.y + inputVector.getY()), (this.z + inputVector.getZ()));
        return newVector;
    }
}
