// YOU RAN WANG 2233844
package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTest 
{
    @Test
    public void getXRerurn1()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        assertEquals(1, testVector.getX(), 0.0001);
    }

    @Test
    public void getYReturn2()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        assertEquals(2, testVector.getY(), 0.0001);
    }

    @Test
    public void getZReturn3()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        assertEquals(3, testVector.getZ(), 0.0001);
    }

    @Test
    public void magnitudeReturnSQRT14()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        assertEquals(Math.sqrt(14), testVector.magnitude(), 0.0001);
    }

    @Test
    public void dotProductReturn14()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        Vector3d testVector2 = new Vector3d(1, 2, 3);
        assertEquals(14, testVector.dotProduct(testVector2), 0.0001);
    }

    @Test
    public void addReturnVector246()
    {
        Vector3d testVector = new Vector3d(1, 2, 3);
        Vector3d testVector2 = new Vector3d(1, 2, 3);
        
        assertEquals(2, testVector.add(testVector2).getX(), 0.0001);
        assertEquals(4, testVector.add(testVector2).getY(), 0.0001);
        assertEquals(6, testVector.add(testVector2).getZ(), 0.0001);
    }
}
